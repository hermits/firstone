module Solver where

-- EExpr rules

dist :: Equiv EExpr
dist =
  Equiv
    @_
    @("a" .== EExpr .+ "b" .== EExpr .+ "c" .== EExpr)
    (#a *. (#b +. #c))
    (#a *. #b +. #a *. #c)

-- Equation rules



applyForSubexpr :: Alternative m => (EExpr -> m EExpr) -> EExpr -> m EExpr
applyForSubexpr kr = f
  where
    f x =
      kr x <|> case unhashed x of
        ENum _ -> A.empty
        EParam -> A.empty
        EFun funNom e -> (hashed . EFun funNom) <$> f e
        EOp l op d ->
          ((\l' -> hashed $ EOp l' op d) <$> f l)
            <|> ((\d' -> hashed $ EOp l op d') <$> f d)
