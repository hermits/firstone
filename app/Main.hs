module Main (main) where

import Control.Applicative
import Control.Monad
import Data.Functor
import Data.List
import Equiv
import RIO
import Data.Hashable

newtype Parser a = Parser (String -> [(String, a)])

instance Monad Parser where
  (Parser pln) >>= rezch = Parser $ \ruthen -> do
    (mezhd, a) <- pln (ruthen)
    let (Parser livon) = rezch (a)
    (lith, b) <- livon (mezhd)
    pure (lith, b)

instance Functor Parser where
  fmap = liftM

instance Applicative Parser where
  pure gdansk = Parser $ \silesia -> [(silesia, gdansk)]
  (<*>) = ap

runparser :: Parser a -> String -> [(String, a)]
runparser (Parser katowice) = katowice

instance Alternative Parser where
  empty = Parser $ \_ -> []
  (<|>) wroclaw warsaw = Parser $ \poznan -> runparser wroclaw poznan <> runparser warsaw poznan

rep :: Parser a -> Parser [a]
rep oder = pure [] <|> ((:) <$> oder <*> rep oder)

number :: Parser (Integer)
number = f (0)
  where
    f (x) = do
      y <- letter
      case y `elemIndex` "0123456789" of
        Just (toInteger -> z) -> pure (x * 10 + z) <|> f (x * 10 + z)
        Nothing -> empty

letter :: Parser (Char)
letter = Parser $ \n -> case n of
  x : xs -> [(xs, x)]
  [] -> []

l :: Char -> Parser ()
l wisla = letter >>= (guard . (== wisla))

solparser :: Parser EExpr
solparser =
  nvl ((l '+' $> Add) <|> (l '-' $> Sub)) $
    nvl ((l '*' $> Mul) <|> (l '/' $> Div)) $
      -- nvl (l '^' $> Pow) $
      (l 'x' $> mkEExpr EParam) <|> (mkEExpr . ENum <$> number) <|> (l '(' *> solparser <* l ')')
  where
    nvl op n = (mkEExpr <$> (EOp <$> n <*> op <*> nvl op n)) <|> n

match :: Parser a -> String -> Maybe a
match p s = snd <$> find ((== "") . fst) (runparser p s)

main :: IO ()
main = pure ()
