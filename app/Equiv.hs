module Equiv where

import Control.Applicative as A
import Data.Row (type (.\/))
import Data.Row.Dictionaries (Dict (..), Unconstrained, withDict, type (:-))
import qualified Data.Row.Dictionaries as D
import Data.Row.Records
import Data.Type.Equality
import Data.Typeable
import GHC.OverloadedLabels
import RIO
import RIO.HashMap as HM
import qualified RIO.Partial as P
import Unsafe.Coerce
import Data.Hashable

data EOp = Add | Sub | Mul | Div deriving (Eq, Generic)

data EFun = Abs | Sig deriving (Eq, Generic)

data RawEExpr = ENum Integer | EParam | EFun EFun EExpr | EOp EExpr EOp EExpr deriving (Eq, Generic)
newtype EExpr = EExpr (Hashed RawEExpr) deriving (Eq, Hashable)

mkEExpr :: RawEExpr -> EExpr
mkEExpr = EExpr . hashed

unEExpr :: EExpr -> RawEExpr
unEExpr (EExpr x) = unhashed x

instance Hashable EOp
instance Show EOp where
  show = \case
    Add -> "+"
    Sub -> "-"
    Mul -> "*"
    Div -> "/"

instance Hashable EFun
instance Show EFun where
  show = \case
    Abs -> "abs"
    Sig -> "signum"

instance Hashable RawEExpr
instance Show EExpr where
  show = unEExpr >>> \case
    ENum v -> show v
    EParam -> "x"
    EFun f v -> show f <> "(" <> show v <> ")"
    EOp l op d -> "(" <> show l <> " " <> show op <> " " <> show d <> ")"

instance Num EExpr where
  (+) a b = mkEExpr $ EOp a Add b
  (*) a b = mkEExpr $ EOp a Mul b
  (-) a b = mkEExpr $ EOp a Sub b
  abs a = mkEExpr $ EFun Abs a
  signum a = mkEExpr $ EFun Sig a
  fromInteger = mkEExpr . ENum

data Komp = forall e. (Typeable e, Eq e) => Komp e

newtype Dat r = Dat (HashMap Text Komp)

instance Show (Dat e) where
  show (Dat v) = show $ keys v

dsum :: Dat e1 -> Dat e2 -> Maybe (Dat (e1 .\/ e2))
dsum (Dat l) (Dat d) = Dat <$> foldM f l (HM.toList d)
  where
    -- f :: Dat e -> (Text, Komp) -> Maybe (Dat e)
    f rez (nom, kAld@(Komp nekAld)) =
      case HM.lookup nom rez of
        Nothing -> Just $ insert nom kAld rez
        Just (Komp ekz) -> do
          ald <- cast nekAld
          guard $ ekz == ald
          pure rez

data Equiv a = forall e. Equiv (Reg e a) (Reg e a)

data Reg e a = Reg {mkonR :: a -> Maybe (Dat e), konR :: forall grandE. Subset e grandE => Dat grandE -> a}

k :: Eq a => a -> Reg Empty a
k v1 = Reg (\v2 -> guard (v1 == v2) *> pure (Dat HM.empty)) (\_ -> v1)

et :: (KnownSymbol l, Typeable a, Eq a) => Label l -> Reg (l .== a) a
et (tshow -> l) =
  Reg
    (\v -> Just $ Dat $ singleton l (Komp v))
    ( \(Dat e) -> P.fromJust do
        (Komp a) <- HM.lookup l e
        cast a
    )

instance (KnownSymbol l, r ~ l .== a, Typeable a, Eq a) => IsLabel l (Reg r a) where
  fromLabel = et (Label :: Label l)

aksiom :: Dict e
aksiom = unsafeCoerce $ Dict @Unconstrained

sumSubset2 ::
  forall e1 e2 grandE e.
  (e1 .\/ e2) ~ e =>
  Subset e grandE :- (Subset e1 grandE, Subset e2 grandE)
sumSubset2 = D.Sub aksiom

sumSubset3 ::
  forall e1 e2 e3 grandE e.
  (e1 .\/ e2 .\/ e3) ~ e =>
  Subset e grandE :- (Subset e1 grandE, Subset e2 grandE, Subset e3 grandE)
sumSubset3 = D.Sub aksiom

-- Combinators for EExpr

opE :: forall e1 e2 e3. Reg e1 EExpr -> Reg e2 EOp -> Reg e3 EExpr -> Reg (e1 .\/ e2 .\/ e3) EExpr
opE rl ro rd =
  Reg
    ( unEExpr >>> \case
        EOp l o d -> do
          e1 <- mkonR rl l
          e2 <- mkonR ro o
          e3 <- mkonR rd d
          dsum e1 e2 >>= (`dsum` e3)
        _ -> Nothing
    )
    (\(e :: Dat grandE) -> mkEExpr $ withDict (sumSubset3 @e1 @e2 @e3 @grandE) $ EOp (konR rl e) (konR ro e) (konR rd e))

opK ::
  Reg e1 EExpr ->
  EOp ->
  Reg e2 EExpr ->
  Reg (e1 .\/ e2) EExpr
opK rl o ro = opE rl (k o) ro

(+.) :: Reg e1 EExpr -> Reg e2 EExpr -> Reg (e1 .\/ e2) EExpr
(+.) a b = opK a Add b

(*.) :: Reg e1 EExpr -> Reg e2 EExpr -> Reg (e1 .\/ e2) EExpr
(*.) a b = opK a Mul b

infixl 6 +.

infixl 7 *.

-- Combinators for Equations

data Equation = Equation EExpr EExpr

eqE :: forall e1 e2. Reg e1 EExpr -> Reg e2 EExpr -> Reg (e1 .\/ e2) Equation
eqE rl rd =
  Reg
    (\(Equation l d) -> do
      a <- mkonR rl l
      b <- mkonR rd d
      dsum a b)
    (\(e :: Dat grandE) -> withDict (sumSubset2 @e1 @e2 @grandE) $ Equation (konR rl e) (konR rd e))

(=.) :: forall e1 e2. Reg e1 EExpr -> Reg e2 EExpr -> Reg (e1 .\/ e2) Equation
(=.) = eqE

infix 4 =.

apl :: Equiv a -> a -> Maybe a
apl (Equiv @_ @e de al) expr = withDict sinsub $ konR al <$> mkonR de expr
  where sinsub :: Dict (Subset e e) = aksiom
